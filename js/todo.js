const todoForm = document.querySelector(".todoForm"),
      todoInput = todoForm.querySelector(".todoInput");

function controlSubmit(event){
  event.preventDefault();
}

function init(){
  todoForm.addEventListener("submit",controlSubmit);
}
init();