const form = document.querySelector(".js-form"),
  input = form.querySelector("input");
const welcomeBox = document.querySelector(".welcomeBox"),
  welcomeText = welcomeBox.querySelector(".welcomeText"),
  userChangeBtn = welcomeBox.querySelector(".changeUserName");
// const todoForm = document.querySelector(".todoForm");


const SHOWING_ON = "flex",
  SHOWING_OFF = "none";

function loadName() {
  const CURRENTUSER = localStorage.getItem("currentUser");
  if (CURRENTUSER === null) {
    input.style.display = SHOWING_ON;
    welcomeBox.style.display = SHOWING_OFF;
  } else {
    paintText(CURRENTUSER);
  }
}

function paintText(username) {
  const realHours = localStorage.getItem("hours");
  welcomeText.innerText = `Hello ${username}, ${realHours <= 12 ? "Good morning!" : "Good afternoon!"}`;

  input.style.display = SHOWING_OFF;
  welcomeBox.style.display = SHOWING_ON;
  todoForm.style.display = SHOWING_ON;
}

function nameInput(event) {
  event.preventDefault();
  localStorage.setItem("currentUser", input.value);
  input.value = "";
  loadName();
}

function userChange() {
  input.style.display = SHOWING_ON;
  welcomeBox.style.display = SHOWING_OFF;
  todoForm.style.display = SHOWING_OFF;
  localStorage.removeItem("currentUser");
}

function init() {
  form.addEventListener("submit", nameInput);
  userChangeBtn.addEventListener("click", userChange);
  loadName();
}
init();